package mahPackage;

public class PolinomModel {
    private static int maxp1;
    private static int maxp2;
    private static int imaxp1;
    private static int imaxp2;
    private static Polynomial pQuotient    = new Polynomial();
    private static Polynomial pRemainder   = new Polynomial();

    public Polynomial getQuotient() {
        return pQuotient;
    }

    public Polynomial getRemainder() {
        return pRemainder;
    }

    public static void MaximumPowerPolynomials(Polynomial p1, Polynomial p2) {
        maxp1 = Integer.MIN_VALUE;
        maxp2 = Integer.MIN_VALUE;
        imaxp1 = Integer.MIN_VALUE;
        imaxp2 = Integer.MIN_VALUE;
        for (int i = 0; i< p1 . List . size(); i++) {
            if (maxp1 < p1 . List . get(i) . getPower()) {
                maxp1 = p1 . List . get(i) . getPower();
                imaxp1 = i;
            }
        }
        for (int i = 0; i< p2 . List . size(); i++) {
            if (maxp2 < p2 . List . get(i) . getPower()) {
                maxp2 = p2 . List . get(i) . getPower();
                imaxp2 = i;
            }
        }
        if (maxp1 == Integer.MIN_VALUE)
            maxp1 = -1;
        if (maxp2 == Integer.MIN_VALUE)
            maxp2 = -1;
    }

    public Polynomial Addition(Polynomial p1,Polynomial p2) {
        Polynomial paux = new Polynomial();
        MaximumPowerPolynomials(p1, p2);
        if (maxp1 >= maxp2) {
            for (int i = 0; i < p1 . List . size(); i++) {
                paux = p1;
                for (int j = 0; j < p2. List . size(); j++) {
                    if (p1 . List . get(i) . getPower() == p2 . List . get(j) . getPower()) {
                        Monomial maux = new Monomial(p2 . List . get(j) . getValue(), p1 . List . get(i) . getPower());
                        paux . addMonomial(maux);
                    }
                }
            }
        }
        else {
            for (int i = 0; i < p2 . List . size(); i++) {
                paux = p2;
                for (int j = 0; j < p1. List . size(); j++) {
                    if (p2 . List . get(i) . getPower() == p1 . List . get(j) . getPower()) {
                        Monomial maux = new Monomial(p1 . List . get(j) . getValue(), p2 . List . get(i) . getPower());
                        paux . addMonomial(maux);
                    }
                }
            }
        }

        return paux;
    }

    public Polynomial Substraction(Polynomial p1,Polynomial p2) {
        Polynomial paux = new Polynomial();
        MaximumPowerPolynomials(p1, p2);
        if (maxp1 >= maxp2) {
            for (int i = 0; i < p1 . List . size(); i++) {
                paux = p1;
                for (int j = 0; j < p2. List . size(); j++) {
                    if (p1 . List . get(i) . getPower() == p2 . List . get(j) . getPower()) {
                        Monomial maux = new Monomial(-(p2 . List . get(j) . getValue()), p1 . List . get(i) . getPower());
                        paux . addMonomial(maux);
                    }
                }
            }
        }
        else {
            for (int i = 0; i < p2 . List . size(); i++) {
                paux = p2;
                for (int j = 0; j < p1. List . size(); j++) {
                    if (p2 . List . get(i) . getPower() == p1 . List . get(j) . getPower()) {
                        Monomial maux = new Monomial(p1 . List . get(j) . getValue(), p2 . List . get(i) . getPower());
                        paux . addMonomial(maux);
                    }
                }
            }
        }

        for (int i = 0; i < paux. List . size(); i++) {
            if (paux . List . get(i) . getValue() == 0) {
                paux . List . remove(i);
                i--;
            }

        }

        return paux;
    }

    public Polynomial Multiplication(Polynomial p1, Polynomial p2) {
        Polynomial paux = new Polynomial();
        for (int i = 0; i < p1 . List . size(); i++)
            for (int j = 0; j < p2. List . size (); j++) {
                Monomial m = new Monomial(p1 . List . get(i) . getValue() * p2 . List . get(j) . getValue(),
                        p1 . List . get(i) . getPower() + p2 . List . get(j) . getPower());
                paux . addMonomial(m);
            }

        return paux;
    }

    public void Divider(Polynomial p1, Polynomial p2) {
        Polynomial paux  = new Polynomial();
        Polynomial paux1 = new Polynomial();
        Polynomial paux2 = new Polynomial();
        MaximumPowerPolynomials(p1, p2);
        if (maxp1 >= maxp2) {
            if (p1 . List . get(imaxp1) . getPower() >= p2 . List . get(imaxp2) . getPower()) {
                Monomial m = new Monomial((p1 . List . get(imaxp1) . getValue() / p2 . List . get(imaxp2) . getValue()),
                        (p1 . List . get(imaxp1) . getPower() - p2 . List . get(imaxp2) . getPower()));
                paux . addMonomial(m);
                pQuotient . addMonomial(m);
                paux1 = Multiplication(paux, p2);
                paux2 = Substraction(p1, paux1);
                Divider(paux2, p2);
            }
        }
        else {
            pRemainder = p1;
        }
    }
}
