package mahPackage;

import java.util.ArrayList;

public class Polynomial {

    public ArrayList <Monomial> List = new ArrayList <Monomial> ();

    public void clearPolynomial() {
        List.clear();
    }

    public void addMonomial (Monomial m) {
        int ok = 0;
        if (List . isEmpty())
            List.add(m);
        else {
            for (int i = 0; i < this. List .size(); i++) {
                if (m.getPower() == this . List . get(i) . getPower()) {
                    this . List . get(i) . setValue(this . List. get(i) . getValue() + m . getValue());
                    ok = 1;
                }
            }
            if (ok == 0) {
                List.add(m);
            }
        }
    }
}