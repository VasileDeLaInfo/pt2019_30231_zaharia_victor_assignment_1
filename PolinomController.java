package mahPackage;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

public class PolinomController {
    private PolinomModel poli_model;
    private PolinomView  poli_view;
    Polynomial p1 = new Polynomial();
    Polynomial p2 = new Polynomial();
    Polynomial p3 = new Polynomial();
    Polynomial pQuotient = new Polynomial();
    Polynomial pRemainder = new Polynomial();
    PolinomController(PolinomModel model, PolinomView view) {
        poli_model = model;
        poli_view  = view;

        view . addFirstDerivationListener(new FirstDerivationListener());
        view . addSecondDerivationListener(new SecondDerivationListener());
        view . addFirstIntegrationListener(new FirstIntegrationListener());
        view . addSecondIntegrationListener(new SecondIntegrationListener());
        view . addAdditionListener(new AdditionListener());
        view . addSubstractionListener(new SubstractionListener());
        view . addMultiplicationListener(new MultiplicationListener());
        view . addDividerListener(new DividerListener());

    }

    public void SaveData(int polynomNumber) {
        String input, changedInput;
        String[] splitInput, changedSplitInput;

        if(polynomNumber == 1) {
            input = poli_view . getUserInputPol1();
            if (input . equals("")) {
                System . out . println("Eroare de input la pol1");
                return;
            }
        }
        else {
            input = poli_view . getUserInputPol2();
            if (input . equals("")) {
                System . out . println("Eroare de input la pol2");
                return;
            }
        }
        changedInput = input . replace("-", "+-");
        splitInput = changedInput . split("\\+");
        for (int i = 0; i < splitInput . length; i++) {
            changedSplitInput = splitInput[i] . split("x\\^");
            Monomial m = new Monomial(Integer . parseInt(changedSplitInput[0]), Integer . parseInt(changedSplitInput[1]));
            if(polynomNumber == 1)
                p1 . addMonomial(m);
            else
                p2 . addMonomial(m);
        }
    }

    public int MaximumPower(Polynomial p) {
        Polynomial paux = new Polynomial();
        paux = p;
        int max = Integer.MIN_VALUE;

        for (int i = 0; i< paux . List . size(); i++) {
            if (max < paux . List . get(i) . getPower())
                max = paux . List . get(i) . getPower();
        }

        if (max == Integer.MIN_VALUE)
            max = -1;
        return max;
    }

    public int[] PopulateArrayOfPolynomials(Polynomial p, int size) {
        int[] array = new int[size + 1];
        Arrays.fill(array, 0);
        Polynomial paux = new Polynomial();
        paux = p;

        for (int i = 0; i <= size; i++)
            for (int j = 0; j < paux . List . size(); j++)
                if (i == paux . List . get(j) . getPower())
                    array[i] = paux . List . get(j) . getValue();

        return array;
    }

    class FirstDerivationListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            p1.clearPolynomial();
            SaveData(1);
            StringBuilder result = new StringBuilder();
            int maxPower = MaximumPower(p1);
            int[] array;
            array = PopulateArrayOfPolynomials(p1, maxPower);

            result . append(maxPower * array[maxPower] + "x^" + (maxPower - 1));

            for (int i = maxPower - 1; i > 0; i--)
                if (array[i] != 0) {
                    if (i * array[i] > 0)
                        result .append("+");
                    result . append(i * array[i] + "x^" + (i - 1));
                }
            poli_view . setResult(result.toString());
        }
    }

    class SecondDerivationListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            p2.clearPolynomial();
            SaveData(2);
            StringBuilder result = new StringBuilder();
            int maxPower = MaximumPower(p2);
            int[] array;
            array = PopulateArrayOfPolynomials(p2, maxPower);

            result . append(maxPower * array[maxPower] + "x^" + (maxPower - 1));

            for (int i = maxPower - 1; i > 0; i--)
                if (array[i] != 0) {
                    if (i * array[i] > 0)
                        result .append("+");
                    result . append(i * array[i] + "x^" + (i - 1));
                }
            poli_view . setResult(result.toString());

        }
    }

    class FirstIntegrationListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            p1.clearPolynomial();
            SaveData(1);
            StringBuilder result = new StringBuilder();
            int maxPower = MaximumPower(p1);
            int[] array;
            array = PopulateArrayOfPolynomials(p1, maxPower);

            result . append("(" + array[maxPower] + "/" + (maxPower + 1) + ")x^" + (maxPower + 1));

            for (int i = maxPower - 1; i > 0; i--)
                if (array[i] != 0) {
                    result . append("+(");
                    result . append(array[i] + "/" + (i + 1));
                    result . append(")x^" + (i + 1));
                }

            if (array[0] > 0)
                result .append("+");
            result . append(array[0] + "x^1");

            poli_view . setResult(result.toString());
        }
    }

    class SecondIntegrationListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            p2.clearPolynomial();
            SaveData(2);
            StringBuilder result = new StringBuilder();
            int maxPower = MaximumPower(p2);
            int[] array;
            array = PopulateArrayOfPolynomials(p2, maxPower);

            result . append("(" + array[maxPower] + "/" + (maxPower + 1) + ")x^" + (maxPower + 1));

            for (int i = maxPower - 1; i > 0; i--)
                if (array[i] != 0) {
                    result . append("+(");
                    result . append(array[i] + "/" + (i + 1));
                    result . append(")x^" + (i + 1));
                }

            if (array[0] > 0)
                result .append("+");
            result . append(array[0] + "x^1");

            poli_view . setResult(result.toString());
        }
    }

    class AdditionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            SaveData(1);
            SaveData(2);
            p3 = poli_model . Addition(p1, p2);

            StringBuilder result = new StringBuilder();
            int maxPower = MaximumPower(p3);
            int[] array;
            array = PopulateArrayOfPolynomials(p3, maxPower);

            result . append(array[maxPower] + "x^" + (maxPower));

            for (int i = maxPower - 1; i >= 0; i--)
                if (array[i] != 0) {
                    if (array[i] > 0)
                        result .append("+");
                    result . append(array[i] + "x^" + i);
                }
            poli_view . setResult(result.toString());
        }
    }

    class SubstractionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            SaveData(1);
            SaveData(2);
            p3 = poli_model . Substraction(p1, p2);

            StringBuilder result = new StringBuilder();
            int maxPower = MaximumPower(p3);
            int[] array;
            boolean ok = false;
            array = PopulateArrayOfPolynomials(p3, maxPower);

            if (array[maxPower] != 0) {
                result . append(array[maxPower] + "x^" + (maxPower));
                ok = true;
            }

            for (int i = maxPower - 1; i >= 0; i--)
                if (array[i] != 0) {
                    ok = true;
                    if (array[i] > 0)
                        result .append("+");
                    result . append(array[i] + "x^" + i);
                }
            if (ok)
                poli_view . setResult(result.toString());
            else
                poli_view . setResult("0");
        }
    }

    class MultiplicationListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            p1.clearPolynomial();
            p2.clearPolynomial();
            SaveData(1);
            SaveData(2);
            p3 = poli_model . Multiplication(p1, p2);

            StringBuilder result = new StringBuilder();
            int maxPower = MaximumPower(p3);
            int[] array;
            array = PopulateArrayOfPolynomials(p3, maxPower);

            result . append(array[maxPower] + "x^" + (maxPower));

            for (int i = maxPower - 1; i >= 0; i--)
                if (array[i] != 0) {
                    if (array[i] > 0)
                        result .append("+");
                    result . append(array[i] + "x^" + i);
                }

            poli_view . setResult(result.toString());
        }
    }

    class DividerListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            p1.clearPolynomial();
            p1.clearPolynomial();
            SaveData(1);
            SaveData(2);
            poli_model . Divider(p1, p2);
            pQuotient = poli_model . getQuotient();
            pRemainder = poli_model . getRemainder();

            StringBuilder result = new StringBuilder();
            int maxPowerQuotient = MaximumPower(pQuotient);
            int[] arrayQuotient;
            arrayQuotient = PopulateArrayOfPolynomials(pQuotient, maxPowerQuotient);

            result . append(arrayQuotient[maxPowerQuotient] + "x^" + (maxPowerQuotient));

            for (int i = maxPowerQuotient - 1; i >= 0; i--)
                if (arrayQuotient[i] != 0) {
                    if (arrayQuotient[i] > 0)
                        result .append("+");
                    result . append(arrayQuotient[i] + "x^" + i);
                }

            int maxPowerRemainder = MaximumPower(pRemainder);
            if (maxPowerRemainder > -1) {
                int[] arrayRemainder;
                arrayRemainder = PopulateArrayOfPolynomials(pRemainder, maxPowerRemainder);

                result . append("\t Rest: " + arrayRemainder[maxPowerRemainder] + "x^" + (maxPowerRemainder));

                for (int i = maxPowerRemainder - 1; i >= 0; i--)
                    if (arrayRemainder[i] != 0) {
                        if (arrayRemainder[i] > 0)
                            result .append("+");
                        result . append(arrayRemainder[i] + "x^" + i);
                    }
            }
            poli_view . setResult(result.toString());

        }
    }

}
