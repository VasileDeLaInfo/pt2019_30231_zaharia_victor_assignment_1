package mahPackage;


import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

class PolinomView extends JFrame {
    private static final long serialVersionUID = 1024295708187745441L;
    private JTextField inputPolinom1 = new JTextField(40);
    private JTextField inputPolinom2 = new JTextField(40);
    private JTextField outputPolinom = new JTextField(40);
    private JButton    derivarePol1  = new JButton("Derivare");
    private JButton    derivarePol2  = new JButton("Derivare");
    private JButton    integrarePol1 = new JButton("Integrare");
    private JButton    integrarePol2 = new JButton("Integrare");
    private JButton    adunarePoli   = new JButton("Adunare");
    private JButton    scaderePoli   = new JButton("Scadere");
    private JButton    inmultirePoli = new JButton("Inmultire");
    private JButton    impartirePoli = new JButton("Impartire");

    PolinomView() {

        // m_totalTf.setText(m_model.getValue());
        // m_totalTf.setEditable(false);

        JPanel content1 = new JPanel();
        JPanel content2 = new JPanel();
        JPanel content3 = new JPanel();
        JPanel content4 = new JPanel();
        JPanel content = new JPanel();


        content1 . setLayout(new FlowLayout());
        content2 . setLayout(new FlowLayout());
        content3 . setLayout(new FlowLayout());
        content4 . setLayout(new FlowLayout());
        content . setLayout(new BoxLayout(content, BoxLayout.PAGE_AXIS));

        content1 . add(new JLabel("Polinom1:"));
        content1 . add(inputPolinom1);
        content1 . add(derivarePol1);
        content1 . add(integrarePol1);

        content2 . add(new JLabel("Polinom2:"));
        content2 . add(inputPolinom2);
        content2 . add(derivarePol2);
        content2 . add(integrarePol2);

        content3 . add(adunarePoli);
        content3 . add(scaderePoli);
        content3 . add(inmultirePoli);
        content3 . add(impartirePoli);

        content4 . add(new JLabel("Rezultat:"));
        content4 . add(outputPolinom);

        content . add(content1);
        content . add(content2);
        content . add(content3);
        content . add(content4);

        this . setContentPane(content);
        this . pack();

        this . setTitle("Sistem de procesare a polinoamelor");
        this . setDefaultCloseOperation(JFrame . EXIT_ON_CLOSE);
    }

    String getUserInputPol1() {
        return inputPolinom1 . getText();
    }

    String getUserInputPol2() {
        return inputPolinom2 . getText();
    }

    void setResult(String result) {
        outputPolinom . setText(result);
    }


    void addFirstDerivationListener(ActionListener deriv) {
        derivarePol1 . addActionListener(deriv);
    }

    void addSecondDerivationListener(ActionListener deriv) {
        derivarePol2 . addActionListener(deriv);
    }

    void addFirstIntegrationListener(ActionListener integr) {
        integrarePol1 . addActionListener(integr);
    }

    void addSecondIntegrationListener(ActionListener integr) {
        integrarePol2 . addActionListener(integr);
    }

    void addAdditionListener(ActionListener add) {
        adunarePoli . addActionListener(add);
    }

    void addSubstractionListener(ActionListener substr) {
        scaderePoli . addActionListener(substr);
    }

    void addMultiplicationListener(ActionListener mul) {
        inmultirePoli . addActionListener(mul);
    }

    void addDividerListener(ActionListener div) {
        impartirePoli . addActionListener(div);
    }
}